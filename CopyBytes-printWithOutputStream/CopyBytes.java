/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CopyBytes;

/**
 *
 * @author obzetish <obzentish.github.io>
 */
 
import java.io.FileInputStream; // subclass of abstract InputStream implements Closeable, AutoCloseable
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyBytes {

    public static void main(String[] args) throws IOException {

        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            in = new FileInputStream("canto.txt");
            out = new FileOutputStream("outagain.txt"); // side effect
            int c;

            while ((c = in.read()) != -1) {
                System.out.print((char) c); // dont show utf-8 char
                out.write(c);
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
