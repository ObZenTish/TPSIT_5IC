# TPSIT 5IC
<p align="center">repository with TPSIT exercises
</p>

---

<p align="center">
  <b>"The child is grown, the dream is gone and I've become comfortably numb "</b><br>
  <a href="https://open.spotify.com/track/7Fg4jpwpkdkGCvq1rrXnvx">Pink Floyd, Confortably Numb</a>
  <br><br>
  <img src="https://i.imgur.com/Z9u2fCo.gif">
</p>
