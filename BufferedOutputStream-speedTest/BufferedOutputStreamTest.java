/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BufferedOutputStream;

/**
 *
 * @author obzetish <obzentish.github.io>
 */

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BufferedOutputStreamTest {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLUE = "\u001B[34m";

    public static void main(String[] args) {

        String filename = "output.txt";
        String output = "«Papé Satàn, pape Satàn aleppe!»,\n"
                + "cominciò Pluto con la voce chioccia;\n"
                + "e quel savio gentil, che tutto seppe,\n"
                + "\n"
                + "disse per confortarmi: «Non ti noccia\n"
                + "la tua paura; ché, poder ch'elli abbia,\n"
                + "non ci torrà lo scender questa roccia».\n"
                + "";
        BufferedOutputStream bos = null;

        long startTime = System.nanoTime();
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            bos.write(output.getBytes());
        } catch (FileNotFoundException fnfe) {
            System.out.println("File not found" + fnfe);
        } catch (IOException ioe) {
            System.out.println("Error while writing to file" + ioe);
        } finally {
            try {
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
            } catch (IOException e) {
                System.out.println("Error while closing streams" + e);
            }
        }
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("Time with .flush()");
        System.out.println(ANSI_BLUE + "Time in millisecond : " + estimatedTime + " ms" + ANSI_RESET);
        System.out.println("____________________________________");
        System.out.println(ANSI_RED + "Time in second : " + estimatedTime / 1000 + " s" + ANSI_RESET);
        System.out.println("____________________________________");
        System.out.println("");

        startTime = System.nanoTime();
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            bos.write(output.getBytes());
        } catch (FileNotFoundException fnfe) {
            System.out.println("File not found" + fnfe);
        } catch (IOException ioe) {
            System.out.println("Error while writing to file" + ioe);
        } finally {
            try {
                if (bos != null) {
                    //bos.flush();
                    bos.close();
                }
            } catch (IOException e) {
                System.out.println("Error while closing streams" + e);
            }
        }
        estimatedTime = System.nanoTime() - startTime;
        System.out.println("Time without .flush()");
        System.out.println(ANSI_BLUE + "Time in millisecond : " + estimatedTime + " ms" + ANSI_RESET);
        System.out.println("____________________________________");
        System.out.println(ANSI_RED + "Time in second : " + estimatedTime / 1000 + " s" + ANSI_RESET);
        System.out.println("____________________________________");
    }
}
