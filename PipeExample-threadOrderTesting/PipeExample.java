/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PipeExample;

/**
 *
 * @author obzetish <obzentish.github.io>
 */
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class PipeExample {

    public static void main(String[] args) throws IOException, InterruptedException {

        final PipedOutputStream output = new PipedOutputStream();
        final PipedInputStream input = new PipedInputStream(output);
        final BufferedReader standard = new BufferedReader(new InputStreamReader(System.in));

        Thread thread1 = new Thread(new Runnable() {

            String s;

            @Override
            public void run() {
                try {
                    s = standard.readLine();
                    output.write(s.getBytes());
                } catch (IOException e) {
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int data = input.read();
                    while (data != -1) {
                        System.out.print((char) data);
                        data = input.read();
                    }
                } catch (IOException e) {
                }
            }
        });

        //no matter the order of starting, the program will work anyway
        thread2.start();
        TimeUnit.SECONDS.sleep(20); //added delay
        thread1.start();

        // changing the order of thread start work just fine, even with some delay
    }
}
